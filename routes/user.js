const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/user");
const { Authentication } = require("../middlewares/auth");

router.post("/register", userControllers.Register);
router.post("/login", userControllers.Login);
router.get("/", Authentication, userControllers.GetAll);

module.exports = router;