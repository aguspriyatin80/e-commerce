const mongoose = require('mongoose');
const { Schema } = mongoose;
const { encryptPassword } = require("../helpers/bcrypt");

const userSchema = new Schema({
    username: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        match: [
            /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
            "Password minimum eight characters, at least one letter and one number",
        ],
    },
    // profile: { type: Schema.Types.ObjectId, ref: "Profile"},
}, { timestamps: true, versionKey: false });

// pre, post hooks
userSchema.pre("save", async function(next) {
    let user = this;

    if (user.password && user.isModified("password")) {
        user.password = await encryptPassword(user.password)
    }
    next();
});

const user = mongoose.model("User", userSchema);

exports.User = user;